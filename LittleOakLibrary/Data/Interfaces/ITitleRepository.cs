﻿using LittleOakLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LittleOakLibrary.Interfaces
{
    public interface ITitleRepository : IRepository<TitleModel>
    {
        IEnumerable<TitleModel> GetMostPopularBooks(int count = 5);
    }
}
