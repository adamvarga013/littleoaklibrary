﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LittleOakLibrary.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IBookRepository Books { get; }
        ILibrarianRepository Librarians { get; }
        IRentalRepository Rentals { get; }
        IRenterRepository Renters { get; }
        ITitleRepository Titles { get; }
        int Complete();
    }
}
