﻿using LittleOakLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LittleOakLibrary.Interfaces
{
    public interface IRentalRepository : IRepository<RentalModel>
    {
        IEnumerable<RentalModel> GetRentalsWithUrgentDueDates();
        IEnumerable<RentalModel> GetRentalsByBookId(int bookId);
    }
}
