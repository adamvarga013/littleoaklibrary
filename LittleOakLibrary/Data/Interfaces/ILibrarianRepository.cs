﻿using LittleOakLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LittleOakLibrary.Interfaces
{
    public interface ILibrarianRepository : IRepository<LibrarianModel>
    {
    }
}
