﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LittleOakLibrary.Interfaces
{
    public interface IRepository<T> where T: class
    {
        T Get(int id);
        void Add(T entityToAdd);
        void Remove(T entityToRemove);
        IEnumerable<T> GetAll();
    }
}
