﻿using LittleOakLibrary.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LittleOakLibrary.Data
{
    public class LibraryContext : IdentityDbContext
    {
        public LibraryContext(DbContextOptions<LibraryContext> opt) :
            base(opt)
        {
        }

        public DbSet<BookModel> Books { get; set; }
        public DbSet<LibrarianModel> Librarians { get; set; }
        public DbSet<RentalModel> Rentals { get; set; }
        public DbSet<RenterModel> Renters { get; set; }
        public DbSet<TitleModel> Titles { get; set; }
    }
}
