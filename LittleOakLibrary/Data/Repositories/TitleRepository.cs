﻿using LittleOakLibrary.Interfaces;
using LittleOakLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LittleOakLibrary.Data.Repositories
{
    public class TitleRepository : Repository<TitleModel>, ITitleRepository
    {
        public LibraryContext LibraryContext
        {
            get { return context as LibraryContext; }
        }

        public TitleRepository(LibraryContext context)
            :base(context)
        {
        }

        public IEnumerable<TitleModel> GetMostPopularBooks(int count = 5)
        {
            return LibraryContext.Titles.OrderByDescending(x => x.NumberOfRentals).Take(count).ToList();
        }
    }
}
