﻿using LittleOakLibrary.Interfaces;
using LittleOakLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LittleOakLibrary.Data.Repositories
{
    public class RenterRepository : Repository<RenterModel>, IRenterRepository
    {
        public LibraryContext LibraryContext
        {
            get { return context as LibraryContext; }
        }

        public RenterRepository(LibraryContext context)
            :base(context)
        {
        }

        public IEnumerable<RenterModel> GetRentersWithGivenNumberOfOverdues(int numberOfOverdues)
        {
            return LibraryContext.Renters.Where(x => x.NumberOfOverdues >= numberOfOverdues).ToList();
        }
    }
}
