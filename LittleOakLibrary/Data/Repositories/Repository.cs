﻿using LittleOakLibrary.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LittleOakLibrary.Models
{
    public abstract class Repository<T> : IRepository<T> where T : class
    {
        protected readonly DbContext context;

        public Repository(DbContext context)
        {
            this.context = context;
        }

        public T Get(int id)
        {
            return context.Set<T>().Find(id);
        }

        public void Add(T entityToAdd)
        {
            context.Set<T>().Add(entityToAdd);
        }

        public void Remove(T entityToRemove)
        {
            context.Set<T>().Remove(entityToRemove);
        }

        public IEnumerable<T> GetAll()
        {
            return context.Set<T>().ToList();
        }
    }
}
