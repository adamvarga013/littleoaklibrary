﻿using LittleOakLibrary.Data;
using LittleOakLibrary.Data.Repositories;
using LittleOakLibrary.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LittleOakLibrary.Models
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly LibraryContext context;

        public IBookRepository Books { get; private set; }
        public ILibrarianRepository Librarians { get; private set; }
        public IRentalRepository Rentals { get; private set; }
        public IRenterRepository Renters { get; private set; }
        public ITitleRepository Titles { get; private set; }

        public UnitOfWork(LibraryContext context)
        {
            this.context = context;

            Books = new BookRepository(this.context);
            Librarians = new LibrarianRepository(this.context);
            Rentals = new RentalRepository(this.context);
            Renters = new RenterRepository(this.context);
            Titles = new TitleRepository(this.context);
        }

        public int Complete()
        {
            return context.SaveChanges();
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}
