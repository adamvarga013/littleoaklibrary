﻿using LittleOakLibrary.Interfaces;
using LittleOakLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LittleOakLibrary.Data.Repositories
{
    public class LibrarianRepository : Repository<LibrarianModel>, ILibrarianRepository
    {
        public LibraryContext LibraryContext
        {
            get { return context as LibraryContext; }
        }

        public LibrarianRepository(LibraryContext context)
            :base(context)
        {

        }
    }
}
