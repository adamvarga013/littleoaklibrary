﻿using LittleOakLibrary.Interfaces;
using LittleOakLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LittleOakLibrary.Data.Repositories
{
    public class RentalRepository : Repository<RentalModel>, IRentalRepository
    {
        public LibraryContext LibraryContext
        {
            get { return context as LibraryContext; }
        }

        public RentalRepository(LibraryContext context)
            :base(context)
        {
        }

        public IEnumerable<RentalModel> GetRentalsWithUrgentDueDates()
        {
            return LibraryContext.Rentals.Where(x => x.DueDate <= DateTime.Now.AddDays(3)).ToList();
        }

        public IEnumerable<RentalModel> GetRentalsByBookId(int bookId)
        {
            return LibraryContext.Rentals.Where(x => x.BookId == bookId).ToList();
        }
    }
}
