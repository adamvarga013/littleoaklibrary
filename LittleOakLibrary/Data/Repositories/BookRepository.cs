﻿using LittleOakLibrary.Data;
using LittleOakLibrary.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LittleOakLibrary.Models
{
    public class BookRepository : Repository<BookModel>, IBookRepository
    {
        public LibraryContext LibraryContext
        {
            get { return context as LibraryContext; }
        }

        public BookRepository(LibraryContext littleOakContext)
            :base(littleOakContext)
        {
        }

        public IEnumerable<BookModel> GetBooksByAuthor(string author)
        {
            return LibraryContext.Books.Where(x => x.Author.ToUpper().Contains(author.ToUpper())).ToList();
        }

        public IEnumerable<BookModel> GetBooksByTitle(string title)
        {
            List<int> titleIdsWhichMatch = LibraryContext.Titles.Where(x => x.Title.ToUpper().Contains(title.ToUpper())).Select(x => x.Id).ToList();

            var books = from book in LibraryContext.Books
                    where titleIdsWhichMatch.Any(id => id == book.TitleId)
                    select book;

            return books.ToList();
        }

        public IEnumerable<BookModel> GetAvailableBooks()
        {
            List<int> availableBookIds = LibraryContext.Titles.Where(x => x.PcsAvailable > 0).Select(x => x.Id).ToList();

            var books = from book in LibraryContext.Books
                        where availableBookIds.Any(id => id == book.TitleId)
                        select book;

            return books;
        }
    }
}
