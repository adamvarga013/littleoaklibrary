﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using LittleOakLibrary.Models;
using LittleOakLibrary.SiteModels;
using LittleOakLibrary.Data;
using LittleOakLibrary.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;

namespace LittleOakLibrary.Controllers
{
    public class HomeController : Controller
    {
        private readonly RoleManager<IdentityRole> roleManager;
        private readonly UserManager<IdentityUser> userManager;
        Logic logic;
        IndexSiteModel indexSiteModel;
        NewRentalSiteModel newRentalSiteModel;
        NewBookSiteModel newBookSiteModel;
        AllRentalsSiteModel allRentalsSiteModel;

        public HomeController(RoleManager<IdentityRole> roleManager,
            UserManager<IdentityUser> userManager, LibraryContext context)
        {
            this.roleManager = roleManager;
            this.userManager = userManager;

            //Librarian();
            //Librarian2();

            logic = new Logic(context);
            indexSiteModel = new IndexSiteModel(logic);
            newRentalSiteModel = new NewRentalSiteModel(logic);
            newBookSiteModel = new NewBookSiteModel(logic);
            allRentalsSiteModel = new AllRentalsSiteModel(logic);
        }

        public async Task<IActionResult> Librarian()
        {
            IdentityRole librarianRole = new IdentityRole()
            {
                Name = "Librarians"
            };
            await roleManager.CreateAsync(librarianRole);
            return RedirectToAction("Index");
        }

        public async Task<IActionResult> Librarian2()
        { 
            var firstuser = userManager.Users.FirstOrDefault();
            await userManager.AddToRoleAsync(firstuser, "Librarians");
            return RedirectToAction("Index");
        }

        public IActionResult AddLibrarian()
        {
            //setRoles();
            Librarian();
            Librarian2();
            return RedirectToAction("Index");
        }

        //private void setRoles()
        //{
        //    using (var rm = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(new ApplicationDbContext())))
        //    using (var um = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext())))
        //        foreach (var item in userRoles)
        //        {
        //            if (!rm.RoleExists(item.Value))
        //            {
        //                var roleResult = rm.Create(new IdentityRole(item.Value));
        //                if (!roleResult.Succeeded)
        //                    throw new ApplicationException("Creating role " + item.Value + "failed with error(s): " + roleResult.Errors);
        //            }
        //            var user = um.FindByName(item.Key);
        //            if (!um.IsInRole(user.Id, item.Value))
        //            {
        //                var userResult = um.AddToRole(user.Id, item.Value);
        //                if (!userResult.Succeeded)
        //                    throw new ApplicationException("Adding user '" + item.Key + "' to '" + item.Value + "' role failed with error(s): " + userResult.Errors);
        //            }
        //        }
        //}

        [AllowAnonymous]
        public IActionResult Index()
        {
            return View(indexSiteModel);
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        [HttpGet]
        [Authorize(Roles = "Librarians")]
        public IActionResult NewRental()
        {
            return View("NewRental", newRentalSiteModel.LoadNewViewModel());
        }

        [HttpPost]
        [Authorize(Roles = "Librarians")]
        public IActionResult NewRental(NewRentalSiteViewModel request)
        {
            request.RenterEmail = User.Identity.Name;
            if (newRentalSiteModel.CreateRental(request))
            {
                return RedirectToAction(nameof(Index));
            }
            return View();                       
        }
        
        [HttpPost]
        [Authorize(Roles = "Librarians")]
        public IActionResult NewBook(NewBookSiteViewModel request)
        {
            if (newBookSiteModel.CreateNewBook(request))
            {
                return RedirectToAction(nameof(Index));
            }
            return View();
        }

        [HttpGet]
        [Authorize(Roles = "Librarians")]
        public IActionResult DeleteBook(int bookId)
        {
            indexSiteModel.DeleteBook(bookId);
            return RedirectToAction(nameof(Index));
        }

        [HttpGet]
        [Authorize(Roles = "Librarians")]
        public IActionResult NewBook()
        {
            return View("NewBook", newBookSiteModel.LoadNewViewModel());
        }

        [HttpGet]
        [Authorize(Roles = "Librarians")]
        public IActionResult ReturnBook(int rentalId)
        {
            allRentalsSiteModel.ReturnBook(rentalId);
            allRentalsSiteModel.LoadAllRentals();
            return View("AllRentals", allRentalsSiteModel);
        }

        [HttpGet]
        public IActionResult AllRentals()
        {
            allRentalsSiteModel.LoadAllRentals();
            return View("AllRentals", allRentalsSiteModel);
        }

        [AllowAnonymous]
        public FileContentResult GetImage(int bookId)
        {
            try
            {
                string imageName = logic.GetBookById(bookId).ImageName;
                return File(logic.GetImageBytes(imageName), "image/jpg");
            }
            catch (Exception)
            {

                return null;
            }
           
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
