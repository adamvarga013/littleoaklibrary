﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LittleOakLibrary.Models
{
    [Table("renter")]
    public class RenterModel
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("name")]
        public string Name { get; set; }

        [Column("email_address")]
        public string EmailAddress { get; set; }

        [Column("phone_number")]
        public string PhoneNumber { get; set; }

        [Column("number_of_rentals")]
        public int NumberOfRentals { get; set; }

        [Column("number_of_overdues")]
        public int NumberOfOverdues { get; set; }
    }
}
