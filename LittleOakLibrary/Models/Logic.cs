﻿using LittleOakLibrary.Data;
using LittleOakLibrary.Interfaces;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace LittleOakLibrary.Models
{
    public class Logic
    {
        static Random rnd = new Random();
        private IUnitOfWork unitOfWork;
        CloudBlobContainer container;

        public Logic(LibraryContext context)
        {
            unitOfWork = new UnitOfWork(context);
            ConnectToAzureBlobStorage();
        }

        #region GetAll methods
        public IEnumerable<BookModel> GetAllBooks(bool showInactiveBooks = false)
        {
            if (showInactiveBooks)
            {
                return unitOfWork.Books.GetAll();
            }
            else
            {
                return unitOfWork.Books.GetAll().Where(b => !b.IsInactive);
            }
        }

        public IEnumerable<RentalModel> GetAllRentals()
        {
            return unitOfWork.Rentals.GetAll();
        }

        public IEnumerable<RenterModel> GetAllRenters()
        {
            return unitOfWork.Renters.GetAll();
        }

        public IEnumerable<TitleModel> GetAllTitles()
        {
            return unitOfWork.Titles.GetAll();
        }
        #endregion

        #region Get methods

        public BookModel GetBookById(int id)
        {
            return unitOfWork.Books.Get(id);
        }

        public RentalModel GetRentalById(int id)
        {
            return unitOfWork.Rentals.Get(id);
        }

        public RenterModel GetRenterById(int id)
        {
            return unitOfWork.Renters.Get(id);
        }

        public TitleModel GetTitleById(int id)
        {
            return unitOfWork.Titles.Get(id);
        }

        public IEnumerable<BookModel> GetAvailableBooks()
        {
            return unitOfWork.Books.GetAvailableBooks();
        }

        public RenterModel GetRenterByEmailAddress(string email)
        {
            return unitOfWork.Renters.GetAll().Where(x => x.EmailAddress.ToUpper() == email.ToUpper()).First();
        }

        #endregion

        #region Remove methods

        public void RemoveBookByEntity(BookModel bookToRemove)
        {
            if(!IsBookRented(bookToRemove.Id))
            {
                unitOfWork.Books.Remove(bookToRemove);
                unitOfWork.Titles.Get(bookToRemove.TitleId).PcsAvailable -= 1;
                unitOfWork.Complete();
            }
            else
            {
                // TODO: Ki van kölcsönözve, így nem lehet törölni!
            }

        }

        public void RemoveBookById(int id)
        {
            if (!IsBookRented(id))
            {
                BookModel bookToRemove = GetBookById(id);
                bookToRemove.IsInactive = true;
                unitOfWork.Titles.Get(bookToRemove.TitleId).PcsAvailable -= 1;
                unitOfWork.Complete();
            }
            else
            {
                // TODO: Ki van kölcsönözve, így nem lehet törölni!
            }
        }

        #endregion


        public void AddBook(BookModel bookModel)
        {
            int ean = 978;
            int group = rnd.Next(10, 100);
            int publisher = rnd.Next(1000, 10000);
            int title = rnd.Next(100, 1000);
            int checkDigit = rnd.Next(0, 10);

            bookModel.Isbn = ean.ToString() + "-" + group.ToString() + "-" + publisher.ToString() + "-" + title.ToString() + "-" +checkDigit.ToString();

            unitOfWork.Books.Add(bookModel);
            unitOfWork.Titles.Get(bookModel.TitleId).PcsAvailable += 1;

            unitOfWork.Complete();
        }

        public void AddRenter(RenterModel renterModel)
        {
            unitOfWork.Renters.Add(renterModel);

            unitOfWork.Complete();
        }

        public bool DoesRenterExist(string email)
        {
            return unitOfWork.Renters.GetAll().Where(x => x.EmailAddress.ToUpper() == email.ToUpper()).ToList().Count() == 1;
        }

        public void MakeRental(RentalModel rentalModel)
        {
            int titleId = unitOfWork.Books.Get(rentalModel.BookId).TitleId;

            var s = unitOfWork.Titles.Get(titleId).PcsAvailable;
            if (s > 0)
            {
                RenterModel renter = unitOfWork.Renters.Get(rentalModel.RenterId);
                if (renter != null)
                {
                    renter.NumberOfRentals += 1;
                }
                unitOfWork.Titles.Get(titleId).PcsAvailable -= 1;
                unitOfWork.Titles.Get(titleId).NumberOfRentals += 1;
                unitOfWork.Rentals.Add(rentalModel);
            }

            unitOfWork.Complete();
        }

        public void ReturnBook(int rentalId)
        {
            RentalModel rentalModel = unitOfWork.Rentals.Get(rentalId);
            int titleId = unitOfWork.Books.Get(rentalModel.BookId).TitleId;

            if (unitOfWork.Rentals.Get(rentalId).IsOverdue)
                unitOfWork.Renters.Get(rentalModel.RenterId).NumberOfOverdues += 1;

            unitOfWork.Rentals.Get(rentalId).IsReturned = true;
            unitOfWork.Rentals.Get(rentalId).IsOverdue = false;
            unitOfWork.Titles.Get(titleId).PcsAvailable += 1;

            unitOfWork.Complete();
        }

        private void ConnectToAzureBlobStorage()
        {
            string storageAccountName = "littleoakblob";
            string storageAccountKey = "I8s9oNG4ztANkejOq281fNg85khBun+aiYGSpTQpo8hNPopJbXtYIAg/0udYJu0U4Ow1rqXL6Hr4k46eO6J+ng==";
            //string path = @"C:\CloudTest";

            var storageAccount = new CloudStorageAccount(new StorageCredentials(storageAccountName, storageAccountKey), true);
            var blobClient = storageAccount.CreateCloudBlobClient();
            container = blobClient.GetContainerReference("littleoakblobcontainer");
            container.CreateIfNotExists();
            container.SetPermissions(new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Blob });

            // test purposes
            /*foreach (var filePath in Directory.GetFiles(path, "*.*", SearchOption.AllDirectories))
            {
                string[] data = filePath.Split("\\");
                string file = data[data.Length - 1];
                CloudBlockBlob blob = container.GetBlockBlobReference(file);
                //CloudBlockBlob blob = container.GetBlockBlobReference(filePath);
                blob.Properties.ContentType = "image/jpg";
                blob.UploadFromFile(filePath);
            }*/
        }

        private CloudBlockBlob DownloadFromAzureBlobStorage(string fileName)
        {
            CloudBlockBlob blockBlob = container.GetBlockBlobReference(fileName);
            blockBlob.Properties.ContentType = "image/jpg";
            return blockBlob;
        }

        public void AddTitle(string title)
        {
            unitOfWork.Titles.Add(new TitleModel { Title = title, PcsAvailable = 0, NumberOfRentals = 0 });
            unitOfWork.Complete();
        }

        public void UploadToAzureBlobStorage(byte[] image, int length, string name)
        {
            CloudBlockBlob blockBlob = container.GetBlockBlobReference(name);
            blockBlob.Properties.ContentType = "image/jpg";

            /*using (var fileStream = System.IO.File.OpenRead(path))
            {
                blockBlob.UploadFromStream(fileStream);
            }*/

            blockBlob.UploadFromByteArray(image, 0, length);
        }

        public byte[] GetImageBytes(string imageName)
        {
            CloudBlockBlob blockBlob = DownloadFromAzureBlobStorage(imageName);
            byte[] image = new byte[blockBlob.StreamWriteSizeInBytes];
            blockBlob.DownloadToByteArray(image, 0);
            return image;
        }

        /*public string GetImageContentType(string imageName)
        {
            CloudBlockBlob blockBlob = DownloadFromAzureBlobStorage(imageName);
            return blockBlob.Properties.ContentType;
        }*/

        private bool IsBookRented(int bookId)
        {
            return unitOfWork.Rentals.GetRentalsByBookId(bookId).Where(x => x.IsReturned == false).ToList().Count == 1;
        }
    }
}
