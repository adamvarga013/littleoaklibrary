﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LittleOakLibrary.Models
{
    [Table("title")]
    public class TitleModel
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("title")]
        public string Title { get; set; }

        [Column("pcs_available")]
        public int PcsAvailable { get; set; }

        [Column("number_of_rentals")]
        public int NumberOfRentals { get; set; }
    }
}
