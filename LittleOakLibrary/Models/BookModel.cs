﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LittleOakLibrary.Models
{
    [Table("book")]
    public class BookModel
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("title_id")]
        public int TitleId { get; set; }

        [Column("isbn")]
        public string Isbn { get; set; }

        [Column("author")]
        public string Author { get; set; }

        [Column("year_of_publishing")]
        public int YearOfPublishing { get; set; }

        [Column("publisher")]
        public string Publisher { get; set; }

        [Column("is_inactive")]
        public bool IsInactive { get; set; }

        [Column("image_name")]
        public string ImageName { get; set; }
    }
}
