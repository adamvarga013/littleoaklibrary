﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LittleOakLibrary.Models
{
    [Table("rental")]
    public class RentalModel
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Column("book_id")]
        public int BookId { get; set; }

        [Column("renter_id")]
        public int RenterId { get; set; }

        [Column("due_date")]
        public DateTime DueDate { get; set; }

        [Column("is_overdue")]
        public bool IsOverdue { get; set; }

        [Column("is_returned")]
        public bool IsReturned { get; set; }
    }
}
