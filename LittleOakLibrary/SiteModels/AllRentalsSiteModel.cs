﻿using LittleOakLibrary.Models;
using LittleOakLibrary.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LittleOakLibrary.SiteModels
{
    public class AllRentalsSiteModel
    {
        public IEnumerable<RentalViewModel> RentalList { get; set; }
        public Logic Logic;

        public AllRentalsSiteModel(Logic logic)
        {
            Logic = logic;
        }

        private IEnumerable<RentalViewModel> ConvertToRentalViewModel(IEnumerable<RentalModel> rental, IEnumerable<BookModel> books, IEnumerable<TitleModel> titles)
        {
            List<RentalViewModel> viewModels = new List<RentalViewModel>();
            foreach (var item in rental)
            {
                BookModel book = books.SingleOrDefault(b => b.Id == item.BookId);
                TitleModel title = titles.SingleOrDefault(t => t.Id == book.TitleId);
                RenterModel renter = Logic.GetRenterById(item.RenterId);
            

                viewModels.Add(new RentalViewModel()
                {
                    BookName = title.Title,
                    BookAuthor = book.Author,
                    BookImageName = book.ImageName,
                    BookId = item.BookId,
                    DueDate = item.DueDate,
                    Id = item.Id,
                    RenterName = renter.Name,
                    RenterEmail = renter.EmailAddress,
                    RenterPhoneNumber = renter.PhoneNumber,
                    IsReturned = item.IsReturned

                });
            }
            return viewModels;
        }

        public void LoadAllRentals()
        {
            RentalList = ConvertToRentalViewModel(Logic.GetAllRentals(), Logic.GetAllBooks(true), Logic.GetAllTitles());
        }

        public void ReturnBook(int rentalId)
        {
            this.Logic.ReturnBook(rentalId);
        }
    }

}
