﻿using LittleOakLibrary.Models;
using LittleOakLibrary.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LittleOakLibrary.SiteModels
{
    public class IndexSiteModel
    {
        public IEnumerable<BookViewModel> BookList { get; set; }
        public Logic Logic;

        public IndexSiteModel(Logic logic)
        {
            Logic = logic;
            BookList = ConvertToBookViewModel(Logic.GetAllBooks());
        }

        private IEnumerable<BookViewModel> ConvertToBookViewModel(IEnumerable<BookModel> books)
        {
            List<BookViewModel> viewModels = new List<BookViewModel>();
            foreach (var item in books)
            {
                viewModels.Add(new BookViewModel()
                {
                    Title = Logic.GetTitleById(item.TitleId).Title,
                    Author = item.Author,
                    Id = item.Id,
                    Publisher = item.Publisher,
                    TitleId = item.TitleId,
                    YearOfPublishing = item.YearOfPublishing,
                    Isbn = item.Isbn,
                    PcsAvailable = Logic.GetTitleById(item.TitleId).PcsAvailable
                });
            }
            return viewModels;
        }

        public void DeleteBook(int bookId)
        {
            this.Logic.RemoveBookById(bookId);
        }
    }
}
