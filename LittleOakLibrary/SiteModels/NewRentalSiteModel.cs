﻿using LittleOakLibrary.Models;
using LittleOakLibrary.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LittleOakLibrary.SiteModels
{
    public class NewRentalSiteModel
    {
        public IEnumerable<BookViewModel> Books;

        private Logic Logic;

        public NewRentalSiteModel(Logic logic)
        {
            this.Logic = logic;
            Books = ConvertToBookViewModel(Logic.GetAllBooks());
        }

        public NewRentalSiteViewModel LoadNewViewModel()
        {
            return new NewRentalSiteViewModel()
            {
                DueDate = DateTime.Now,
                Books = Logic.GetAvailableBooks().Select(b => new BookViewModel()
                {
                    Author = b.Author,
                    Id = b.Id,
                    Publisher = b.Publisher,
                    Title = Logic.GetTitleById(b.TitleId).Title,
                    TitleId = b.TitleId,
                    YearOfPublishing = b.YearOfPublishing,
                })
            };
        }

        public bool CreateRental(NewRentalSiteViewModel request)
        {
            try
            {
                if (!Logic.DoesRenterExist(request.EmailAddress))
                {
                    Logic.AddRenter(new RenterModel
                    {
                        EmailAddress = request.EmailAddress,
                        Name = request.Name,
                        NumberOfOverdues = 0,
                        NumberOfRentals = 1,
                        PhoneNumber = request.PhoneNumber
                    });
                }

                this.Logic.MakeRental(new RentalModel()
                {
                    RenterId = Logic.GetRenterByEmailAddress(request.EmailAddress).Id,
                    BookId = request.BookId,
                    DueDate = request.DueDate,
                    IsOverdue = false,
                    IsReturned = false
                });
                return true;

            }
            catch (Exception e)
            {
                return false;
            }

        }

        private IEnumerable<BookViewModel> ConvertToBookViewModel(IEnumerable<BookModel> books)
        {
            List<BookViewModel> viewModels = new List<BookViewModel>();
            foreach (var item in books)
            {
                viewModels.Add(new BookViewModel()
                {
                    Author = item.Author,
                    Id = item.Id,
                    Publisher = item.Publisher,
                    TitleId = item.TitleId,
                    YearOfPublishing = item.YearOfPublishing,
                    Title = Logic.GetTitleById(item.TitleId).Title
                });
            }
            return viewModels;
        }

    }
}
