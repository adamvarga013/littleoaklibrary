﻿using LittleOakLibrary.Models;
using LittleOakLibrary.ViewModels;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace LittleOakLibrary.SiteModels
{
    public class NewBookSiteModel
    {
        public BookViewModel Book { get; set; }
        public IFormFile imageToUpload { get; set; }
        public object TitleId { get; private set; }

        private Logic Logic;

        public NewBookSiteModel(Logic logic)
        {
            this.Logic = logic;
            this.Book = new BookViewModel();
        }

        public NewBookSiteViewModel LoadNewViewModel()
        {
            NewBookSiteViewModel bookViewModel = new NewBookSiteViewModel();
            return bookViewModel;
        }

        public bool CreateNewBook(NewBookSiteViewModel request)
        {
            if (request != null && !string.IsNullOrWhiteSpace(request.ImageName) && !string.IsNullOrWhiteSpace(request.Title) && request.ImageToUpload != null)
            {
                byte[] picture = new byte[request.ImageToUpload.Length];
                using (var stream = request.ImageToUpload.OpenReadStream())
                {
                    stream.Read(picture, 0, (int)request.ImageToUpload.Length);
                }

                Logic.AddTitle(request.Title);
                int titleId = Logic.GetAllTitles().SingleOrDefault(t => t.Title == request.Title).Id;
                Logic.AddBook(new BookModel()
                {
                   Author = request.Author ,
                   ImageName = request.ImageName,
                   Publisher = request.Publisher,
                   YearOfPublishing = request.YearOfPublishing,
                   TitleId = titleId
                });

                Logic.UploadToAzureBlobStorage(picture, (int)request.ImageToUpload.Length, request.ImageName);

                var s = Logic.GetAllBooks();
                var ss = Logic.GetAllTitles();
                return true;
            }
            return false;
        }
    }
}
