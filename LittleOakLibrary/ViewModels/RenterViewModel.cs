﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LittleOakLibrary.ViewModels
{
    public class RenterViewModel
    {
        public int Id { get; set; }
        
        public string Name { get; set; }

        public string EmailAddress { get; set; }
        
        public string PhoneNumber { get; set; }
        
        public int NumberOfOverdues { get; set; }

        public override string ToString()
        {
            return $"{this.Name} email: {this.EmailAddress}, phone: {this.PhoneNumber}, no. of overdues: {this.NumberOfOverdues}";
        }
    }
}
