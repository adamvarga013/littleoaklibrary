﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LittleOakLibrary.ViewModels
{
    public class BookViewModel
    {
        public int Id { get; set; }
        
        public int TitleId { get; set; }
                
        public string Author { get; set; }
        
        public int YearOfPublishing { get; set; }
        
        public string Publisher { get; set; }

        public string Title { get; set; }

        public string Isbn { get; set; }

        public int PcsAvailable { get; set; }

        public override string ToString()
        {
            return $"{this.Id}, {this.TitleId}, {this.Author}, {this.YearOfPublishing}, {this.Publisher}";            
        }
    }
}
