﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LittleOakLibrary.ViewModels
{
    public class NewBookSiteViewModel
    {
        public string Title { get; set; }

        public string Author { get; set; }

        [Range(1000, 2018)]
        public int YearOfPublishing { get; set; }

        public string Publisher { get; set; }

        public string ImageName { get; set; }

        public IFormFile ImageToUpload { get; set; }

    }
}
