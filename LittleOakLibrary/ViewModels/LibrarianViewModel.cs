﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LittleOakLibrary.ViewModels
{
    public class LibrarianViewModel
    {
        
        public int Id { get; set; }
    
        public string Username { get; set; }
        
        public string Password { get; set; }

        public override string ToString()
        {
            return $"{this.Username} [{this.Id}]";
        }

    }
}
