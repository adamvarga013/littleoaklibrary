﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LittleOakLibrary.ViewModels
{
    public class TitleViewModel
    {
        public int Id { get; set; }
        
        public string Title { get; set; }
        
        public int PcsAvailable { get; set; } 
        
        public int NumberOfRentals { get; set; }

        public override string ToString()
        {
            return $"{this.Title} available: {this.PcsAvailable} pcs";
        }
    }
}
