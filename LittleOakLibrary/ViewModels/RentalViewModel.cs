﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LittleOakLibrary.ViewModels
{
    public class RentalViewModel
    {
        public int Id { get; set; }

        public string BookName { get; set; }

        public string BookAuthor { get; set; }

        public bool IsReturned { get; set; }

        public string BookImageName { get; set; }

        public int BookId { get; set; }

        public string RenterName { get; set; }

        public string RenterEmail { get; set; }

        public string RenterPhoneNumber { get; set; }

        public DateTime DueDate { get; set; }

        public override string ToString()
        {
            return $"{this.BookId}\t{this.RenterName}\t{this.DueDate}";
        }
    }
}
