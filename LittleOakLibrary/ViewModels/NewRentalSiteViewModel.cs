﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LittleOakLibrary.ViewModels
{
    public class NewRentalSiteViewModel
    {
        public IEnumerable<BookViewModel> Books { get; set; }

        public int BookId { get; set; }

        public string RenterEmail { get; set; }

        public DateTime DueDate { get; set; }

        public string Name { get; set; }

        public string EmailAddress { get; set; }

        public string PhoneNumber { get; set; }
    }
}
