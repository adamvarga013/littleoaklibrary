﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace LittleOakLibrary.Migrations
{
    public partial class AddedISBN : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "isbn",
                table: "book",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "isbn",
                table: "book");
        }
    }
}
