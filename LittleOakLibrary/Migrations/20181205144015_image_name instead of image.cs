﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace LittleOakLibrary.Migrations
{
    public partial class image_nameinsteadofimage : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "image",
                table: "book");

            migrationBuilder.AddColumn<string>(
                name: "image_name",
                table: "book",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "image_name",
                table: "book");

            migrationBuilder.AddColumn<byte[]>(
                name: "image",
                table: "book",
                nullable: true);
        }
    }
}
